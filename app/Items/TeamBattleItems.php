<?php

namespace App\Items;

class TeamBattleItems
{
    public $haveIGotTeam;
    public $isTeamBattleBegin;
    public $my_team_id;
    public $team1;
    public $team2;
    public $my_team_battle;
}