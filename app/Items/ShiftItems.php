<?php

namespace App\Items;

class ShiftItems
{
    public $shifts;
    public $pharmacy;
    public $shift_code;
    public $shift_close_code;
}
