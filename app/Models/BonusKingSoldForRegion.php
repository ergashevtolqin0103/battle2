<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusKingSoldForRegion extends Model
{
    use HasFactory;

    protected $guarded = [];
}
