<!-- Required jquery and libraries -->
<script src="{{asset('mobile/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('mobile/js/popper.min.js')}}"></script>
<script src="{{asset('mobile/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- cookie js -->
<script src="{{asset('mobile/js/jquery.cookie.js')}}"></script>

<!-- Swiper slider  js-->
<script src="{{asset('mobile/vendor/swiper/js/swiper.min.js')}}"></script>

<!-- Customized jquery file  -->
<script src="{{asset('mobile/js/main.js')}}"></script>
<script src="{{asset('mobile/js/color-scheme-demo.js')}}"></script>


<!-- page level custom script -->
<script src="{{asset('mobile/js/app.js')}}"></script>