<div class="modal fade" id="ustoz-shogird" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content"
            style="background-image: url('/promo/dist/img/promo/bg2.png');background-repeat: no-repeat;">
            <div class="modal-body p-0">
                <div class="modal-header">
                    <img src="{{asset('mobile/ustoz/ustb.webp')}}" width="111%" style="border-radius:15px;margin-left: -20px;margin-top:-18px;position:relative">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="opacity: 5;position:absolute;top:8px;right:10px;">
                        <img src="{{asset('mobile/xclose.png')}}" width="30px">
                    </button>
            </div>
                <livewire:ustoz-shogird />
            </div>
        </div>
    </div>
</div>
